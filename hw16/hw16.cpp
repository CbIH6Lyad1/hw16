﻿#include <iostream>
#include <time.h>
#include <stdio.h>


int main()
{
    const int N = 5;

    int array[N][N];

    for (int i = 0; i < N; i = i + 1)
    {
        for (int j = 0; j < N; j = j + 1)
        {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int index = int(buf.tm_mday) % N;
    int sum = 0;

        for (int j = 0; j < N; j = j + 1)
        {
           sum = sum + array[index][j];
           
        }
        std::cout << sum;
    }